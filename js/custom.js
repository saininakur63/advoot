$(window).on('load',function(){
      $('#countryModal,#otp1,#welcomeModal,#syncModal,#loginModal').modal('show');
  });

$(function(){


  //welcome screen choose intrest
  $('.choose-interest a').click(function(e){
    e.preventDefault();
    $(this).addClass('active');
  })

  $('.move-down').click(function(e){
    e.preventDefault();
    nextOffset = $(this).parent('section').next('section').offset().top;
    $('body,html').animate({scrollTop: nextOffset}, 1500);
  })
  $('.compare-slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1366,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 481,
        settings: {
          arrows:false,
          slidesToShow: 2
        }
      }
    ]
  });


  $('.ott').slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1366,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 481,
        settings: {
          arrows:false,
          slidesToShow: 2
        }
      }
    ]
  });


  $('.full-slide').slick({
    dots: true,
    autoplay: true,
    arrows:false
  });
  $('.product-slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1366,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          arrows:false,
          slidesToShow: 1
        }
      }
    ]
  });
    $('.cast-slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          arrows:false,
          slidesToShow: 1
        }
      }
    ]
  });

// Brand slider
   $('.brand-logo').slick({
    dots: false,
    infinite: true,
    slidesToShow: 13,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1439,
        settings: {
          slidesToShow: 18
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 8
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 10 
        }
      },
      {
        breakpoint: 767,
        settings: {
          arrows:false,
          slidesToShow: 6
        }
      }
    ]
  });

  //movies filter tabs 
  $('.tab-list a').click(function(e){
    e.preventDefault();
    currIdx = $(this).parent('li').index();
    $('.tab-list a').removeClass('active');
    $(this).addClass('active');
    $('.filter-product-box .ftr-tabs').hide();
    $('.filter-product-box .ftr-tabs:eq('+ currIdx +')').show();
  });

  //Back to top 
  $(window).scroll(function(){
     if($(window).scrollTop()>300){
        $('.bac2top').show();
      }else{
        $('.bac2top').hide();
      }
  })
  if($(window).scrollTop()>300){
    $('.bac2top').show();
  }else{
    $('.bac2top').hide();
  }

  $('.bac2top').click(function(e){
    e.preventDefault();
    $('body,html').animate({scrollTop: 0}, 1500);
  });


  // Profile page
  $('.profile-tabs a').click(function(e){
    e.preventDefault();
    $('.profile-tabs a').removeClass('active');
    $(this).addClass('active');
    $('.p-tabs').hide();
    $(this.hash).show();
    offTop = $(this.hash).offset().top;
    if($(window).width()<768){
      $('body,html').animate({scrollTop:offTop},1000)
    }
  });


});


